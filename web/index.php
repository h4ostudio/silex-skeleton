<?php

$app = require_once __DIR__.'/../src/boot.php';

include __DIR__.'/../src/controllers/mainController.php';
use Symfony\Component\HttpFoundation\Response;
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $page = 404 == $code ? '404.html.twig' : '500.html.twig';

    return new Response($app['twig']->render('errors/'.$page, array('code' => $code)), $code);
});

$app->run();