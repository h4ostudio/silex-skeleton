<?php

$app=require_once __DIR__.'/../src/boot.php';

use Symfony\Component\HttpKernel\Debug\ErrorHandler;
use Symfony\Component\HttpKernel\Debug\ExceptionHandler;

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
ErrorHandler::register();
if ('cli' !== php_sapi_name()) {
    ExceptionHandler::register();
}



$app['debug'] = true;

include __DIR__.'/../src/controllers/mainController.php';

$app->run();