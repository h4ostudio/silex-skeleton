<?php 
$options = array(
      'scope'=>array('publish_stream'),
      'redirect_uri'=>''
    );

$app->register(new Tobiassjosten\Silex\Provider\FacebookServiceProvider(), array(
        'facebook.app_id'     => 'efef',
        'facebook.secret'     => 'fefqfeq',
    ));


$app->before(function() use($app, $options) {
    $app['twig']->addGlobal('facebook_login_url',$app['facebook']->getLoginUrl($options));
    $app['twig']->addGlobal('facebook_logout_url',$app['facebook']->getLogoutUrl());
    
   try
   {
        $user = $app['facebook']->getUser();
        $app['twig']->addGlobal('facebook_user',$user);
        $app['session']->set('facebook_user', $user);

        if (!$user)
        {            
            if($app['request']->getRequestUri()!=$app['url_generator']->generate('facebook_login'))
            {
                return $app->redirect($app['url_generator']->generate('facebook_login'));               
            }
        }
   }
   catch (\FacebookApiException $e)
   {        
        if($app['request']->getRequestUri()!=$app['url_generator']->generate('facebook_login'))
        {
            return $app->redirect($app['url_generator']->generate('facebook_login'));            
        }
   }
});

$app->match('/facebook_login',function() use($app){
    return $app['twig']->render('facebook/facebook-login.html.twig');    
    })->bind('facebook_login');