<?php

namespace Todo\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class TareaType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('title', 'text', array('label' => 'Titulo'));
		$builder->add('body','textarea',array('label' => 'Descripcion'));
	}

 	public function getName()
	{
		return "tarea";
	}
}