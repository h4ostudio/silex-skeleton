<?php

$app->match('/', function() use($app){
	$tasks=$app['db']->fetchAll('select * from tareas');
	return $app['twig']->render('home.html.twig', array('tasks'=>$tasks));
})->bind('home');

